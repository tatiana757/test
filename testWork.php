﻿<?

    class newArray {
        private $old_arr;
        private $required;

        function __construct($old_arr,$required) {
            $this->old_arr = $old_arr;
            $this->required = $required;
        }

        public function newArr() {
            $i=0;
            foreach($this->old_arr as $key => $value){
                if(is_array($value)){
                    foreach ($value as $key2 => $val) {
                        $i++;
                        if(!empty($val)) {
                            if (!in_array($key2, $this->required)) {
                                $keys= $key2 .$i;
                                $new_arr[$keys] = $val;
                            }
                        }
                    }
                }
                else{
                    $i++;
                    if(!empty($value)) {
                        if (!in_array($key, $this->required)) {
                            $keys= $key .$i;
                            $new_arr[$keys] = $value;
                        }
                    }
                }
            }
            print_r($new_arr);
        } 
    }

    $old_arr = array(
        'a' => '11',
        'b' => '22',
        'c' => array(
            'd' => '33',
            'e' => '44',
            'f' => '',
            'g' => '66',
            'h' => '77',
        ),
        'i' => '0',
        'j' => '99',
    );

    $required = array('a', 'e', 'i', 'o', 'u', 'y');

    $arr = new newArray($old_arr, $required);
    echo '<pre>';
    print_r($arr->newArr());
	echo '</pre>';
?>